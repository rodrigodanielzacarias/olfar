"use strict";

// PADRÃO
var express = require("express");
var router = express.Router();

router.get("/", function(req, res, next) {
  res.end("hello!");
});

router.use(function(req, res, next) {
  /**
   * Se nao encontrar nada, envia e encerra conexao
   */
  // res.send('park//nada encontrado');
  res.end("");
});

module.exports = router;
