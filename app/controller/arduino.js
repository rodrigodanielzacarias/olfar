'use strict';

// PADRÃO
var express = require('express');
var router = express.Router();

router.get('/', function (req, res, next) {
    console.log('\n  GET Cancela -> ', req.query);
    // res.send('park//' + JSON.stringify(req.query));
    res.end('hello!');
});

router.post('/', function (req, res, next) {
    console.log('\n  POST Cancela -> ', req.body);
    // res.send('park//' + JSON.stringify(req.body));
    res.end();
});

router.use(function (req, res, next) {
    /**
     * Se nao encontrar nada, envia e encerra conexao
     */
    // res.send('park//nada encontrado');
    res.end('');
});

module.exports = router;