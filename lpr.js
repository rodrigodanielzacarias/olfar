const onvif = require('node-onvif');
const fs = require('fs');

var contador = 0;

console.log('Start the discovery process.');
// Find the ONVIF network cameras.
// It will take about 3 seconds.
onvif.startProbe().then((device_info_list) => {
    console.log(device_info_list.length + ' devices were found.');
    // Show the device name and the URL of the end point.
    device_info_list.forEach((info) => {
        console.log(' LPR encontrado --> ', info.xaddrs);

        let device = new onvif.OnvifDevice({
            xaddr: '' + info.xaddrs,
            user: 'admin',
            pass: 'admin'
        });

        // console.log('go!');
        // Initialize the OnvifDevice object
        device.init().then(() => {
            // Get the data of the snapshot
            // console.log('fetching the data of the snapshot...');
            return device.fetchSnapshot();
        }).then((res) => {
            // Save the data to a file
            contador = contador + 1;
            fs.writeFileSync('OLFAR_LPR_' + contador + '.jpg', res.body, { encoding: 'binary' });
            console.log(' Done ---> ', 'OLFAR_LPR_' + contador + '.jpg');
        }).catch((error) => {
            // console.error(error);
            // console.log(error);
        });

    });
}).catch((error) => {
    console.error(error);
});


// let device = new onvif.OnvifDevice({
//     xaddr: 'http://192.168.159.10/onvif/device_service',
//     user: 'admin',
//     pass: 'cam@olfar'
// });

// console.log('go!');
// // Initialize the OnvifDevice object
// device.init().then(() => {
//     // Get the data of the snapshot
//     console.log('fetching the data of the snapshot...');
//     return device.fetchSnapshot();
// }).then((res) => {
//     // Save the data to a file
//     contador = contador + 1;
//     fs.writeFileSync('OLFAR_LPR_' + contador + '.jpg', res.body, { encoding: 'binary' });
//     console.log(' Done ---> ', 'OLFAR_LPR_' + contador + '.jpg');
// }).catch((error) => {
//     // console.error(error);
//     // console.log(error);
// });