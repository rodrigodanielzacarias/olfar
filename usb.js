var serialport = require("serialport");

serialport.list(function(err, ports) {
  ports.forEach(function(port) {
    console.log(port.comName);
  });
});

/* Connect */
const port = new serialport("COM4", {
  baudRate: 9600
  // parser: new serialport.parsers.Readline("\n")
});

port.on("error", function(err) {
  console.log("Error: ", err.message);
});

port.on("open", function() {
  console.log("PORT Conected");
});

port.on("data", function(data) {
  var hello = data.toString();
  hello = hello.replace(/\r\n/g, "");
  if (hello == "HELLO") {
    console.log("OK");
  }
});

port.write("main screen turn on", function(err) {
  if (err) {
    return console.log("Error on write: ", err.message);
  }
  console.log("message written");
});
