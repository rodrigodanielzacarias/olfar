var request = require('request');
var options = {
    url: 'http://192.168.8.240:80/sis', // <-- Ip do destinatario
    method: 'get',
    body: "cmd=STATUS",
    headers: {
        /**
         * 'Authorization': 'Basic ' + new Buffer("admin:125625").toString('base64')
         */
        Authorization: 'Basic YWRtaW46MTI1NjI1',
    }
}

/** Exemplo de HTML a ser enviado!
* POST / HTTP/1.1
* host: 192.168.16.240
* content-length: 14
* Connection: close
*
* cmd=STATUS
*/

/**
 * Comandos disponiveis
 *   ABRE   --> ABRE CANCELA
 *   FECHA  --> FECHA CANCELA
 *   STATUS --> RETORNA STATUS
 *   CFG31  --> ACIONA MODO EMERGENCIA
 *   CFG30  --> RETORNA MODO EMERGENCIA para FUNCIONAMENTO PADRAO
 */

request(options, function (error, response, body) {
    if (!error && response.statusCode == 200) {
        console.log(' ----> Resposta da Cancela: ', JSON.parse(response.body), '\n');
    } else {
        console.log(error);
    }
})