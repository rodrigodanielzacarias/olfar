"use strict";

var express = require("express");
var session = require("express-session");
// var parseurl = require('parseurl');
var path = require("path");
// var favicon = require('serve-favicon');
var logger = require("morgan");
var cookieParser = require("cookie-parser");
var bodyParser = require("body-parser");

var park = require("./app/controller/arduino");

var app = express();

app.use(logger("dev"));

app.use(require("express-is-ajax-request"));

app.set("trust proxy", 1); // trust first proxy
app.use(
  session({
    secret: "secret",
    resave: true,
    saveUninitialized: true
  })
);

// view engine setup
app.set("views", path.join(__dirname, "app/views"));
app.set("view engine", "ejs");

// uncomment after placing your favicon in /assets
//app.use(favicon(path.join(__dirname, 'assets', 'favicon.ico')));
app.use(
  bodyParser.urlencoded({
    extended: true
  })
);

app.use(bodyParser.json());
app.use(cookieParser());
app.use("/assets", express.static(__dirname + "/assets"));

app.use("/park", park);
/**
 * http://192.168.8.245/park
 *
 */

var son = {
  rfid: 123,
  auth: "bsuaniomaS"
};

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error("Not Found");
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  console.log("\nERROR --------------------- ERROR");
  console.log(err.message);
  res.locals.error = req.app.get("env") === "development" ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.end("Página não existente");
});
// app.listen(3000);

module.exports = app;
